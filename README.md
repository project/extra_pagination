## Extra Pagination

Extra Pagination is a very simple module that provides a new set of pager
items between the last visible pager link and the maximum pager number.
This is mainly done for SEO reasons to make us reach any page in 3-4
clicks maximum. Suggestions and feature requests are welcome.

### Installation and requirements
No setup required. Once you install the module, the extra pager items will
appear.

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders


### Credits:

Adrian Marin ([amarincolas](https://drupal.org/u/amarincolas))
